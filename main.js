// Previous Page Button
function goBack() {
    window.history.back();
}
  
let latitude= "";
let longitude= "";
let pageNum= 1;
let photoNum= 0;
let arrOfURLs= [];

let findCoords = function() {
    function userLocationAvailable(location) {
        latitude = location.coords.latitude
        longitude = location.coords.longitude
        document.getElementById("searchCoords").innerHTML = (`Latitude: ${latitude} and Longitude: ${longitude}`);
    }

    // I dont like this whole section of the function TRY TO CLEAN it UP
    function userLocationBlocked() {
        latitude = 52.370216
        longitude = 4.895168
        document.getElementById("searchCoords").innerHTML = (`Latitude: ${latitude} and Longitude: ${longitude}`);
    }
    // Amsterdam
    if (!navigator.geolocation) {
        latitude = 52.370216
        longitude = 4.895168
    } else {
        navigator.geolocation.getCurrentPosition(userLocationAvailable, userLocationBlocked);
    }
}
// From Randy's demo
let constructURL = function() {
    findCoords();
    return `https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?` +
    `api_key=e2458108983307b9103453f3f9f88596&` +
    `format=json&` +
    `nojsoncallback=1&` +
    `method=flickr.photos.search&` +
    `safe_search=1&` +
    `per_page=5&` +
    `page=1&` +
    `text=Flowers` +
    `${latitude}&` +
    `${longitude}&`
};
// From Randy's demo
let fetchPhotos = function() {
    let url = constructURL();
    fetch(url)
        .then(function(response) {
            return response.json()
        })
        .then(function(data) {
            createImageUrl(data)
            return data
        })
};
// From Randy's demo 
let createImageUrl = function(data) {
    for (let i = 0; i < data.photos.photo.length; i++) {
        arrOfURLs.push("https://farm" + 
        data.photos.photo[i].farm + 
        ".staticflickr.com/" + 
        data.photos.photo[i].server + 
        "/" + data.photos.photo[i].id + 
        "_" + data.photos.photo[i].secret + 
        ".jpg")
    }
    publishPhoto(arrOfURLs);
}
let i = 0
let publishPhoto = function(arrOfURLs) {
    if (i < 5) {
        let URL = document.getElementById("flickrPhotoURL");
        URL.href = arrOfURLs[i]
        let photoLocation = document.getElementById("flickrPhotos");
        photoLocation.src = arrOfURLs[i];i++
    } 
    else {
        i= 0
        publishPhoto(arrOfURLs);
    }
}
fetchPhotos();
